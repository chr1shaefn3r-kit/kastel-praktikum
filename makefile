DOCUMENT_NAME = DoS

# Build the LaTeX document.
all: report

# Remove output directory.
clean:
	rm -rf *.acn *.acr *.alg *.aux *.glg *.glo *.gls *.ist *.lof *.log *.lol *.out *.toc *.bbl *.blg

# Generate PDF output from LaTeX input files.
report:
	pdflatex -interaction=errorstopmode $(DOCUMENT_NAME)
	bibtex $(DOCUMENT_NAME)
	pdflatex -interaction=errorstopmode $(DOCUMENT_NAME)
	pdflatex -interaction=errorstopmode $(DOCUMENT_NAME)
